FROM node:8

RUN mkdir -p /home/node/app
WORKDIR /home/node/app

COPY package.json /home/node/app
RUN yarn

COPY . /home/node/app

EXPOSE 3000

CMD [ "yarn", "start"]
