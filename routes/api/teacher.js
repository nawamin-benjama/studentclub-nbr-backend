const express = require("express");
const router = express.Router();

const Club = require("../../models/club");
const User = require("../../models/user");

// Get all user
router.post("/user/all", function (req, res) {
  // find all user
  User.find({}, function (err, users) {
    if (err) {
      // return error
      return next(err);
    } else {
      // return data
      return res.json({
        success: true,
        users
      });
    }
  });
});

// Get all club
router.post("/club/all", function (req, res) {
  // find all user
  Club.find({}, function (err, clubs) {
    if (err) {
      // return error
      return next(err);
    } else {
      // return data
      return res.json({
        success: true,
        clubs
      });
    }
  });
});




module.exports = router;