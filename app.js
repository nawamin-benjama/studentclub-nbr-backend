const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const session = require("express-session");
const MongoStore = require("connect-mongo")(session);
const cors = require("cors");

const config = require("./config/config");

// connect to MongoDb
mongoose.connect(config.database);
const db = mongoose.connection;

//handle mongo error
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function () {
  console.log("connection success");
});

// use sessions for tracking logins
app.use(
  session({
    secret: config.secret,
    resave: true,
    saveUninitialized: false,
    store: new MongoStore({
      mongooseConnection: db
    })
  })
);

// set secret
app.set("superSecret", config.secret); // secret variable

// parse incoming request
app.use(bodyParser.json({
  limit: "10mb"
}));
// in latest body-parser use like below.
app.use(bodyParser.urlencoded({
  limit: "10mb",
  extended: false
}));

// cross platform
app.use(
  cors({
    // origin: ["http://localhost:8080"],
    origin: ["http://studentclub.vasin.me"],
    methods: ["GET", "POST"],
    credentials: true
  })
);

// include routes
const routes = require("./routes/router");
app.use("/api", routes);

// listen on port 3000
app.listen(config.port, function () {
  console.log("Express app listening on port 3000");
});