const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
  // important field
  studentId: {
    type: String,
    unique: true,
    required: true,
    trim: true,
    index: {
      unique: true
    }
  },
  password: {
    type: String,
    required: true
  },
  permission: {
    type: Number,
    required: true
  },
  // start study
  start_at: {
    type: Date,
    required: true
  },
  // educate level:4 class:3
  education: {
    level: { type: String, trim: true, required: true },
    class: { type: String, trim: true, required: true }
  },

  // Common field //
  prefix: {
    type: String,
    trim: true
  },

  // firstname
  firstname: {
    type: String,
    trim: true
  },
  // lastname
  lastname: {
    type: String,
    trim: true
  },
  // tel
  telephone: {
    type: String,
    trim: true
  },
  // entry club
  club: {
    type: String,
    trim: true
  },

  // Auto filed //
  created_on: {
    type: Date,
    default: Date.now
  }
});

const User = mongoose.model("User", UserSchema);
module.exports = User;
